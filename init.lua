-- shock's insulted plugin

-- hackery provided by daurnimator
-- package.path = "/opt/teamspeak3/plugins/lua_plugin/?.lua;/usr/share/lua/5.2/?.lua;/usr/share/lua/5.2/?/init.lua;/usr/lib/lua/5.2/?.lua;/usr/lib/lua/5.2/?/init.lua;./?.lua"; package.cpath = "/usr/lib/lua/5.2/?.so;/usr/lib/lua/5.2/loadall.so;./?.so"

require("ts3init")
require("insulted/events")

local registeredEvents = {
    onTextMessageEvent = insulted_events.onTextMessageEvent
}
ts3RegisterModule("insulted", registeredEvents)
ts3.printMessageToCurrentTab('insulted loaded.')
